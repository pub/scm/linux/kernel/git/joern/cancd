
VERSION=0.2.2

CFLAGS += -Wall -g -O2 -DVERSION="\"$(VERSION)\""
LDFLAGS += -Wl,--no-as-needed -lpthread -lrt

cancd: cancd.o btree.o

*.o: Makefile btree.h kerncompat.h kernel-list.h

install:
	mkdir -p $(DESTDIR)/usr/sbin
	cp cancd $(DESTDIR)/usr/sbin
	chmod 0755 $(DESTDIR)/usr/sbin/cancd
	mkdir -p $(DESTDIR)/etc/init.d
	cp cancd.init $(DESTDIR)/etc/init.d/cancd
	chmod 0755 $(DESTDIR)/etc/init.d/cancd

clean:
	rm -f cancd cancd.o btree.o

DIST_FILES =		\
	Makefile	\
	cancd.c		\
	cancd.init	\
	cancd.spec	\
	kernel-list.h

DIST_DIR = cancd-$(VERSION)
DIST_ARCHIVE = $(DIST_DIR).tar.gz

dist:
	@rm -rf $(DIST_DIR)
	@mkdir -p $(DIST_DIR)
	@cp $(DIST_FILES) $(DIST_DIR)
	tar -czvf $(DIST_ARCHIVE) $(DIST_DIR)
	@rm -rf $(DIST_DIR)

RPM_TOPDIR = $(CURDIR)
RPMBUILD = $(shell /usr/bin/which rpmbuild 2>/dev/null || /usr/bin/which rpm 2>/dev/null || echo /bin/false)

CHKCONFIG_DEP = $(shell if test -r /etc/SuSE-release; then echo aaa_base; else echo chkconfig; fi)

srpm: dist
	$(RPMBUILD) -bs --define "_sourcedir $(RPM_TOPDIR)" --define "_srcrpmdir $(RPM_TOPDIR)" --define "CANCD_VERSION $(VERSION)" --define "CHKCONFIG_DEP $(CHKCONFIG_DEP)" cancd.spec

rpm: srpm
	$(RPMBUILD) --rebuild --define "CANCD_VERSION $(VERSION)" --define "CHKCONFIG_DEP $(CHKCONFIG_DEP)" "cancd-$(VERSION)-1.src.rpm"

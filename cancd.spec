#
# Spec file for cvsman
#

# Macros
# This one is hardcoded because, well, it belongs there
%define _prefix /usr

Summary: The CA NetConsole Daemon
Name: cancd
Version: %{CANCD_VERSION}
Release: 1
Copyright: GPL
Group: Applications/File
Source: http://oss.oracle.com/projects/cancd/files/source/cancd-%{PACKAGE_VERSION}.tar.gz
URL: http://oss.oracle.com/projects/cancd/
Distribution: CALPG
Vendor: Oracle Corporate Architecture Linux Projects Group
Packager: Joel Becker <joel.becker@oracle.com>

# Needs an initscripts require
Requires: initscripts >= 6.44, %{CHKCONFIG_DEP}

BuildRoot: %{_tmppath}/cancd-%{PACKAGE_VERSION}-%{PACKAGE_RELEASE}-root

%description
This is the CA NetConsole Daemon, a daemon to receive output from the
Linux netconsole driver.

%prep
%setup

%build
make

%install
make DESTDIR="$RPM_BUILD_ROOT" install
mkdir "${RPM_BUILD_ROOT}/etc/rc.d"
mv "${RPM_BUILD_ROOT}/etc/init.d" "${RPM_BUILD_ROOT}/etc/rc.d"

%clean
rm -rf "$RPM_BUILD_ROOT"

%post
/sbin/chkconfig --add cancd

%preun
if [ $1 = 0 ]; then
   /sbin/chkconfig --del cancd
fi

%files
%defattr(-,root,root)
/usr/sbin/cancd
%config /etc/rc.d/init.d/cancd
